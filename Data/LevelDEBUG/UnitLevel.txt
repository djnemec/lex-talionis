# UnitLevel.txt is used to define what units will be part of this level and where they will spawn
# 
# Each unit belongs on its own line
# Syntax:
# New Units:
# team; 0; event_id; class; level; items; position; ai; faction; status (optional)
# - OR -
# Named units:
# team; 1; event_id; unit_id; position; ai
# - OR -
# Created Units:
# team; 2; event_id; class; items; position; ai; faction; status (optional)
# 
# event_id gives the unit a unique id that scripts can use. The unit will not start on the battlefield unless event_id == 0.
# unit_id - unit to load from the units.xml file
# position should be formatted like #,#
# ai refers to what kind of AI the unit should possess.
#
# --------------------------------------------
faction;Althea;Althea;Resistance;Nope
faction;Resistance;Bandit;Resistance;Nope
# Player Characters
player;0;0;Warlock;1;Howl,Searing Blades,Lightning_Bolt,Energy Drink;1,14;None;Resistance
# player;0;0;Archer;5;Poison Bow,Iron Axe,Energy Drink;14,2;None;Resistance
# Enemies
# Generics
player;0;0;Ophie;3,14;None
player;0;0;Prim;4,15;None
enemy;0;0;Dragoon;5;Adamant Lance;4,13;Attack_1;Resistance
#enemy;0;p3_2;Dragoon;5;Mithril Lance;4,13;Pursue;Resistance
# === Reinforcements ===
# Player Characters
# Enemies
# === Triggers ===
