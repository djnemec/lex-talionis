# Event script that is called every time the player's turn starts. Normally handles reinforcements
if;gameStateObj.turncount == 1
    show_layer;1
end
if;gameStateObj.turncount == 2
    hide_layer;1
    change_ai;3,15;Pursue
    add_talk;Ophie;Althea
    give_item;Ophie;Iron Sword
    # show_layer;2
    add_unit;p3_2
end
if;gameStateObj.turncount == 4
    set_cursor;0,0;force_hold
    wait;500
	change_tile_sprite;0,0;Door
    wait;2000
end